<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

ini_set('display_errors', 1);
error_reporting(E_ALL);

require(__DIR__ . '/../chef/vendor/autoload.php');
require(__DIR__ . '/../chef/vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../chef/config/web.php');

(new yii\web\Application($config))->run();
