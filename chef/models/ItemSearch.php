<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Item;

/**
 * ItemSearch represents the model behind the search form about `app\models\Item`.
 */
class ItemSearch extends Item
{
//    public $category;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['itemId', 'unit', 'qty'], 'integer'],
            [['name', 'category'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Item::find();
//        $query->joinWith(['category c']);

        // add conditions that should always apply here
        $this->load($params);



        // grid filtering conditions
        $query->andFilterWhere([
            'itemId' => $this->itemId,
            'unit' => $this->unit,
            'qty' => $this->qty,
//            'item_category.name' => $this->category,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
//            ->andFilterWhere(['like', 'item_category.name', $this->category]);

        $query->limit(1);
        $query->offset(0);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

//        $dataProvider->sort->attributes['category'] = [
//            // The tables are the ones our relation are configured to
//            // in my case they are prefixed with "tbl_"
//            'asc' => ['item_category.name' => SORT_ASC],
//            'desc' => ['item_category.name' => SORT_DESC],
//        ];

//        if (!$this->validate()) {
//             //uncomment the following line if you do not want to return any records when validation fails
//             //$query->where('0=1');
//            return $dataProvider;
//        }

        return $dataProvider;
    }
}
