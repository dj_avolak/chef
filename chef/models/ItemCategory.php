<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_category".
 *
 * @property integer $itemCategoryId
 * @property string $name
 */
class ItemCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['itemCategoryId'], 'required'],
//            [['itemCategoryId'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'itemCategoryId' => 'Category ID',
            'name' => 'Name',
        ];
    }
}
