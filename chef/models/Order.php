<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $orderId
 * @property integer $recipeId
 * @property integer $status
 * @property integer $qty
 * @property string  $createdAt
 */
class Order extends \yii\db\ActiveRecord
{
    const STATUS_OK = 1;
    const STATUS_EXPENSE = 2;
    const STATUS_VOID = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recipeId'], 'integer'],
            [['createdAt', 'qty', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'orderId' => 'Order ID',
            'recipeId' => 'Recipe ID',
            'createdAt' => 'Created At',
            'status' => 'Status',
            'qty' => 'Qty',
        ];
    }

    public function getRecipe()
    {
        return $this->hasOne(Recipe::className(), ['recipeId' => 'recipeId']);
    }

    public function getStatus()
    {
        if ($this->status === self::STATUS_VOID) {
            return 'stornirano';
        }

        return ($this->status === self::STATUS_OK) ? 'zavrsheno' : 'rashod';
    }
}
