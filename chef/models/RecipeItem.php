<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recipe_item".
 *
 * @property integer $recipeItemId
 * @property integer $recipeId
 * @property integer $itemId
 * @property integer $itemQty
 * @property integer $itemUnit
 */
class RecipeItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recipe_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recipeId', 'itemId', 'itemUnit'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recipeItemId' => 'Recipe Item ID',
            'recipeId' => 'Recipe ID',
            'itemId' => 'Item ID',
            'itemQty' => 'Item Qty',
            'itemUnit' => 'Item Unit',
        ];
    }

    public function getItem()
    {
        return $this->hasOne(Item::className(), ['itemId' => 'itemId']);
    }
}
