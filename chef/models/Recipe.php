<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recipe".
 *
 * @property integer $recipeId
 * @property string $name
 * @property string $priceOut
 * @property string $priceIn
 * @property integer $margin
 */
class Recipe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recipe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['priceOut', 'priceIn'], 'number'],
            [['margin'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recipeId' => 'Recipe ID',
            'name' => 'Name',
            'priceOut' => 'Price Out',
            'priceIn' => 'Price In',
            'margin' => 'Margin',
        ];
    }

    public function getItems()
    {
        return $this->hasMany(RecipeItem::className(), ['recipeId' => 'recipeId']);
    }
}
