<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item".
 *
 * @property integer $itemId
 * @property string $name
 * @property integer $unit
 * @property integer $qty
 * @property integer $price
 * @property integer $categoryId
 */
class Item extends \yii\db\ActiveRecord
{
    const QTY_KG = 1;
    const QTY_PIECE = 2;
    const QTY_PACK = 3;
    const QTY_BOTTLE = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit'], 'required'],
            [['unit', 'categoryId'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'itemId' => 'Item ID',
            'name' => 'Name',
            'unit' => 'Unit',
            'qty' => 'Qty',
            'price' => 'Price per unit',
            'categoryId' => 'Category',
        ];
    }

    public function getUnit()
    {
        if ($this->unit === self::QTY_KG) {
            return 'kg';
        }

        return ($this->unit === self::QTY_PIECE) ? 'komada' : 'pakovanja';
    }

    public function in($qty)
    {
        $this->qty = $this->qty + $qty;
        $this->save();
    }

    public function out($qty)
    {
        $this->qty = $this->qty - $qty;
        $this->save();
    }

    public function getCategory()
    {
        return $this->hasOne(ItemCategory::className(), ['itemCategoryId' => 'categoryId']);
    }

    public function scenarios()
    {
        return [
            'default' => ['name', 'unit', 'qty', 'price', 'categoryId'],
        ];
    }
}
