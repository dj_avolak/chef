<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderId', 'recipeId'], 'integer'],
            [['createdAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

//        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (isset($params['OrderSearch']['createdAt'])) {
            $query->andWhere('createdAt BETWEEN "' . $params['OrderSearch']['createdAt'] . ' 00:00:00' . '" AND "' .
                $params['OrderSearch']['createdAt'] . ' 23:59:59"');
        } else {
            $dt = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
            $query->andWhere('createdAt BETWEEN "' . $dt->format('Y:m:d') . ' 00:00:00' . '" AND "' .
                $dt->format('Y:m:d') . ' 23:59:59"');
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'orderId' => $this->orderId,
//            'recipeId' => $this->recipeId,
//            'createdAt' => $this->createdAt,
//        ]);

        return $dataProvider;
    }
}
