<?php

namespace app\controllers;

use app\models\ItemCategory;
use Yii;
use app\models\Item;
use app\models\ItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Adds inventory state to existing one.
     *
     * @return string
     */
    public function actionAddInventory()
    {
        $msg = false;

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstanceByName('inventory');
            $file->saveAs(__DIR__ . '/../inventory.xlsx');
            /* @var \PHPExcel $excell */
            $excell = \PHPExcel_IOFactory::load(__DIR__ . "/../inventory.xlsx");
            $cells = $excell->setActiveSheetIndex(0)->getCellCollection();
            $rowCount = count($cells) / 3;

            try {
                for ($i=1; $i < $rowCount; $i++) {
                    if ($excell->setActiveSheetIndex(0)->getCell('A' . $i)->getValue()) {
                        $item = Item::findOne(['itemId' => $excell->setActiveSheetIndex(0)->getCell('A' . $i)]);
                        $item->in($excell->setActiveSheetIndex(0)->getCell('C' . $i)->getValue());
                        $item->price = $excell->setActiveSheetIndex(0)->getCell('D' . $i)->getValue();
                        $item->save();
                    }
                }
                $msg = "Inventory added successfully";
            } catch (\Exception $e) {
                $msg = "Doslo je do greske prilikom snimanja, detalji su ispod: <br />";
                $msg .= $e->getMessage() . '<br />';
                $msg .= $e->getTraceAsString();
            }
        }

        return $this->render('inventory', [
            'msg' => $msg
        ]);
    }

    /**
     * Sets imported values as new inventory state.
     *
     * @return string
     */
    public function actionSetInventory()
    {
        $msg = false;

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstanceByName('inventory');
            $file->saveAs(__DIR__ . '/../inventory.xlsx');
            /* @var \PHPExcel $excell */
            $excell = \PHPExcel_IOFactory::load(__DIR__ . "/../inventory.xlsx");
            $cells = $excell->setActiveSheetIndex(0)->getCellCollection();
            $rowCount = count($cells) / 3;

            try {
                for ($i=1; $i < $rowCount; $i++) {
                    if ($excell->setActiveSheetIndex(0)->getCell('A' . $i)->getValue()) {
                        $item = Item::findOne(['itemId' => $excell->setActiveSheetIndex(0)->getCell('A' . $i)]);
                        $item->qty = $excell->setActiveSheetIndex(0)->getCell('C' . $i)->getValue();
                        $item->price = $excell->setActiveSheetIndex(0)->getCell('D' . $i)->getValue();
                        $item->save();
                    }
                }
                $msg = "Inventory added successfully";
            } catch (\Exception $e) {
                $msg = "Doslo je do greske prilikom snimanja, detalji su ispod: <br />";
                $msg .= $e->getMessage() . '<br />';
                $msg .= $e->getTraceAsString();
            }
        }

        return $this->render('inventory', [
            'msg' => $msg
        ]);
    }

    public function actionInventory()
    {
        $excell = new \PHPExcel();
        $excell->getProperties()->setCreator("djavolak")
            ->setTitle("Chef inventory list")
            ->setSubject("Chef inventory list");
//            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")

        $items = Item::find()->all();

        $i=1;
        foreach ($items as $item) {
            $excell->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $item->itemId)
                ->setCellValue('B' . $i, $item->name)
                ->setCellValue('C' . $i, $item->qty)
                ->setCellValue('D' . $i, $item->price);
            $i++;
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="inventory.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($excell, 'Excel5');
        $objWriter->save('php://output');

    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Item();
        $categories = ArrayHelper::map(ItemCategory::find()->all(), 'itemCategoryId', 'name');
        $units = [
            Item::QTY_KG => 'kg',
            Item::QTY_PIECE => 'komad',
            Item::QTY_PACK => 'pakovanje',
            Item::QTY_BOTTLE => 'boca',
        ];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'categories' => $categories,
                'units' => $units
            ]);
        }
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $categories = ArrayHelper::map(ItemCategory::find()->all(), 'itemCategoryId', 'name');
        $units = [
            Item::QTY_KG => 'kg',
            Item::QTY_PIECE => 'komad',
            Item::QTY_PACK => 'pakovanje',
            Item::QTY_BOTTLE => 'boca',
        ];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update',[
                'model' => $model,
                'categories' => $categories,
                'units' => $units
            ]);
        }
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
