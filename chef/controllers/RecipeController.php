<?php

namespace app\controllers;

use app\models\Order;
use app\models\RecipeItem;
use Yii;
use app\models\Recipe;
use app\models\Item;
use app\models\RecipeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecipeController implements the CRUD actions for Recipe model.
 */
class RecipeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recipe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecipeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrder()
    {
        $rows = RecipeItem::findAll(['recipeId' => $_POST['recipeId']]);
        foreach ($rows as $row) {
            $item = Item::findOne(['itemId' => $row->itemId]);
            $item->out($row->itemQty);
        }
        $dt = new \DateTime();
        $order = new Order();
        $order->recipeId = $_POST['recipeId'];
        $order->qty = $_POST['qty'];
        $order->status = 1;
        $order->createdAt = $dt->format('Y:m:d H:i:s');
        if (!$order->save()) {
            throw new \Exception('Could not create order.' . print_r($order->attributes, true));
        }
        echo 1;
    }

    public function actionWork()
    {
        return $this->render('work', [
            'recipes' => Recipe::find()->all(),
        ]);
    }

    /**
     * Displays a single Recipe model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recipe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recipe();
        $items = Item::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->processItems($model);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'items' => $items,
            ]);
        }
    }

    /**
     * Updates an existing Recipe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $items = Item::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->removeOldItems($model);
            $this->processItems($model);

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'items' => $items,
            ]);
        }
    }

    protected function removeOldItems($model)
    {
        foreach (RecipeItem::findAll(['recipeId' => $model->recipeId]) as $recipeItem) {
            $recipeItem->delete();
        }
    }

    protected function processItems($model)
    {
        for ($i=0; $i< count(Yii::$app->request->post()['ingredients']); $i++) {
            $recipeItem = new RecipeItem();
            $recipeItem->recipeId = $model->recipeId;
            $recipeItem->itemUnit = 1; //kg
            $recipeItem->itemId = Yii::$app->request->post()['ingredients'][$i];
            $recipeItem->itemQty = Yii::$app->request->post()['qty'][$i];
            if (!$recipeItem->save()) {
                var_dump($recipeItem->getErrors());
                die();
            }
        }
    }

    /**
     * Deletes an existing Recipe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Recipe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recipe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recipe::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRecalculate()
    {
        $this->recalculateInputPriceForRecipes();
    }

    protected function recalculateInputPriceForRecipes()
    {
        $recipes = Recipe::find()->all();
        foreach ($recipes as $recipe) {
            /* @var RecipeItem $recipeItem */
            $price = 0;
            foreach ($recipe->getItems()->all() as $recipeItem) {
                $price += $recipeItem->itemQty * $recipeItem->getItem()->one()->price;
            }
            $recipe->priceIn = $price;
            $recipe->save();
        }

        echo 'success';
    }
}
