<?php

use yii\helpers\Html;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RecipeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Work');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recipe-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <ul>
        <?php foreach ($recipes as $recipe): ?>
        <li><?=$recipe->name?> - <?=$recipe->priceOut?>e
            <a class="orderRecipe" data-id="<?=$recipe->recipeId?>" href="#">Order</a>
            <input type="number" class="orderQty" value="1" />
        </li>
        <?php endforeach; ?>
    </ul>

</div>

<?php
$script = <<< JS
jQuery(document).ready(function() {
    $('.orderRecipe').click(function(e) {
        e.preventDefault();
        var url = location.origin + location.pathname + '?r=recipe/order';
        var data = {
            'recipeId': $(this).data('id'), 
            'qty': $(this).siblings('input').val()
        };
        $.post(url, data, function(JSON) {
            if (JSON == 1) {
                alert('order successfull');
            }
        });
    });
});
JS;
$this->registerJs($script, View::POS_END);
?>