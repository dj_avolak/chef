<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Recipe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recipe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    Ingredients: <br />
    <a class="addIngredient">Add new</a>

    <?php if (!$model->isNewRecord): ?>
    <ul>
        <?php foreach ($model->getItems()->all() as $recipeItem): ?>
        <div class="selectWrapper">
            <select name="ingredients[]">
                <option>-- choose --</option>
                <?php foreach ($items as $item): ?>
                    <option data-price="<?=$item->price?>" value="<?=$item->itemId?>" <?php if ($item->itemId === $recipeItem->itemId) { echo 'selected="selected"'; } ?>><?=$item->name?></option>
                <?php endforeach; ?>
            </select>
            <input class="qty" type="text" name="qty[]" placeholder="qty" value="<?=$recipeItem->itemQty?>" />
            <a href="#" class="removeRecipeItem" alt="removeItem">X</a>
        </div>
<?php /*
        <li><?=$recipeItem->getItem()->one()->name?> - <?=$recipeItem->itemQty?> <a href="#" class="removeRecipe">X</a></li>
        <input type="hidden" name="ingredients[]" value="<?=$recipeItem->itemId?>" />
        <input type="hidden" name="qty[]" value="<?=$recipeItem->itemQty?>" /> */ ?>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>

    <?= $form->field($model, 'priceOut')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'priceIn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'margin')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div class="selectContainer" style="display: none">
    <select name="ingredients[]">
        <option>-- choose --</option>
        <?php foreach ($items as $item): ?>
            <option data-price="<?=$item->price?>" value="<?=$item->itemId?>"><?=$item->name?></option>
        <?php endforeach; ?>
    </select>
    <input class="qty" type="text" name="qty[]" placeholder="qty" />
</div>
<?php
$script = <<< JS
jQuery(document).ready(function() {
    $('.addIngredient').click(function(e) {
        e.preventDefault();
        var item = $('.selectContainer').clone().removeClass('selectContainer').addClass('selectWrapper');
        item.insertAfter($('.addIngredient')).fadeIn();
    });
    
    // @TODO math.....
    $('.recipe-form').on('change', '.qty', function(e) {
        if ($('#recipe-priceout').val() == '') {
            $('#recipe-priceout').val(0);
        }
        if ($('#recipe-pricein').val() == '') {
            $('#recipe-pricein').val(0);
        }
        var price = parseFloat($('#recipe-priceout').val()) + parseFloat($(this).prev().find(':selected').data('price') * parseFloat($(this).val()));
        $('#recipe-priceout').val(price);
        $('#recipe-pricein').val(price);
        console.log($('#recipe-priceout').val() + $(this).prev().find(':selected').data('price') * $(this).val());
        console.log($(this).prev().find(':selected').data('price') * $(this).val());
        console.log(price);
    });
    
    $('.removeRecipeItem').click(function(e) {
        e.preventDefault();
        $(this).parent().fadeOut().remove();
    });
    
});
JS;
$this->registerJs($script, View::POS_END);
?>