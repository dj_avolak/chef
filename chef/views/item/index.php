<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Items');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Item'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Get inventory'), ['inventory'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Add inventory'), ['add-inventory'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Set inventory'), ['set-inventory'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'itemId',
            'name',
            [
                'label' => 'Category',
                'value' => function ($model) {
                    return $model->getCategory()->one()->name;
                }
            ],
//            'qty',
            [
                'label' => 'Qty',
                'value' => function ($model) {
                    return $model->qty;
                }
            ],
            [
                'label' => 'Unit',
                'value' => function ($model) {
                    return $model->getUnit();
                }
            ],
            'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

