<?php

$this->title = Yii::t('app', 'Items');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="item-index">
    <?php if ($msg): ?>
    <p><?=$msg?></p>
    <?php endif; ?>

    <form enctype="multipart/form-data" method="post" action="">

        <label for="inventory">Select file to import</label>
        <input type="file" name="inventory" />

        <input type="submit" value="Upload" />

        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
    </form>
</div>
