<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Order'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>Total sum for displayed orders: <?=$totalSum?></p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'orderId',
            'recipe.name',
//            'status',
            [
                'label' => 'Status',
                'value' => function ($model) {
                    return $model->getStatus();
                }
            ],
            'qty',
            'createdAt',
            'sum',

//            ['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{expense} {void} {delete}',
                'buttons'=>[
                    'expense' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eur"></span>', $url, [
                            'title' => Yii::t('yii', 'Expense'),
                        ]);

                    },
                    'void' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-remove-sign"></span>', $url, [
                            'title' => Yii::t('yii', 'Void'),
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
